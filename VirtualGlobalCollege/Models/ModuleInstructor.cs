﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public class ModuleInstructor
    {
        public int ID { get; set; }
        public int ModuleID { get; set; }
        public int InstructorID { get; set; }

        public virtual Module Modules { get; set; }
        public virtual Instructor Instructors { get; set; }
    }
}
