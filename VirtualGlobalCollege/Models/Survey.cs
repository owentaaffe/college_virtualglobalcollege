﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public enum Range1
    {
        Easy, Normal, Average, Hard, Difficult
    }
    public enum Range2
    {
        Less, Normal, Average, More, Unsuccessful
    }

    public enum Range3
    {
        Striking, Bold, Plain, Bland, Unappealing
    }

    public enum Range4
    {
        Very, Maybe, Undecided, Soso, Never
    }

    public class Survey
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Email Address is required")]
        [DisplayName("Email Address")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
                    ErrorMessage = "Email is not valid.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [Display(Name = "How easy is it to navigate around this website? ")]
        public Range1? Q1 { get; set; }

        [Display(Name = "How easy was it to find what you were looking for on our website? ")]
        public Range1 Q2 { get; set; }

        [Display(Name = "Did it take you more or less time than you expected to find what you were looking for on our website? ")]
        public Range2 Q3 { get; set; }

        [Display(Name = "How visually appealing is our website? ")]
        public Range3 Q4 { get; set; }

        [Display(Name = "How easy is it to understand the information on our website? ")]
        public Range1 Q5 { get; set; }

        [Display(Name = "How likely is it that you would recommend our website to a friend or colleague? ")]
        public Range4 Q6 { get; set; }

        [Display(Name = "Tell us what you liked about our website? ")]
        [DataType(DataType.MultilineText)]
        public string Q7 { get; set; }

        [Display(Name = "What would like to see changed or see added? ")]
        [DataType(DataType.MultilineText)]
        public string Q8 { get; set; }
    }
}
