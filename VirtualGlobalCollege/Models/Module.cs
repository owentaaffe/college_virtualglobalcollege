﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public class Module
    {
        [Display(Name = "Unit No.")]
        public int ModuleID { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [Range(1, 25)]
        public int Credits { get; set; }


        public int CourseID { get; set; }
        public virtual Course Course { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public virtual ICollection<Instructor> Instructors { get; set; }
    }
}
