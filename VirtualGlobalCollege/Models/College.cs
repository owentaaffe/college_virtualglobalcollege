﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public class College
    {
        public int CollegeID { get; set; }
        [Display(Name ="College")]
        public string Title { get; set; }
        public string Description { get; set; }
        public string Administrator { get; set; }
    }
}
