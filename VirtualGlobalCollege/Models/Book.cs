﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{

    public enum Condition
    {
        New, Used
    }

    public class Book
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BookID { get; set; }
        
        [DisplayName("Course")]
        public int CourseId { get; set; }
        [StringLength(150)]
        [Required]
        public string Title { get; set; }
        [DisplayName("Author")]
        public string Author { get; set; }
        [Required]
        [Display(Name = "Price €")]
        public decimal Price { get; set; }
        [Required]
        public Condition? Condition { get; set; }
        public int StockLevel { get; set; }
        public string ImageUrl { get; set; }
        public virtual Course Course { get; set; }

        public decimal price
        {
            get
            {
                var price = Price * 100;
                return (price);
            }

        }

    }
}
