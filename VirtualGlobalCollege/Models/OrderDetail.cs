﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public class OrderDetail
    {
        public int OrderDetailID { get; set; }
        public int StudentID { get; set; }
        public int OrderID { get; set; }
        public int BookID { get; set; }
        public decimal UnitPrice { get; set; }
        public virtual Student Student { get; set; }
        public virtual Book Book { get; set; }
        public virtual Order Order { get; set; }
    }
}
