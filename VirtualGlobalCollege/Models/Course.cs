﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    
    public class Course
    {
        public int CourseID { get; set; }
        [Required]
        [Display(Name = "Courses")]
        public string Title { get; set; }

        [Display(Name = "College")]
        public int CollegeID { get; set; }

        public string Description { get; set; }
        [Required]
        public decimal Fee { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        public int? InstructorID { get; set; }

        public virtual Instructor Instructors { get; set; }

        public virtual ICollection<Module> Modules { get; set; }

        public List<Book> Books { get; set; }
    }
}
