﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public class ContactDetail
    {
        public int Id { get; set; }
        [Required]
        public int StudentID { get; set; }
        [Required]
        [Display(Name = "Address line 1")]
        public string Address1 { get; set; }
        [Display(Name = "Address line 2")]
        public string Address2 { get; set; }
        [Required]
        public string City { get; set; }
        public string County { get; set; }
        public string PostalCode { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }

        public virtual Student Students { get; set; }
    }
}
