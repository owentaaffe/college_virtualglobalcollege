﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public enum Grade
    {
        Fail, Pass, Merit, Distinction
    }
    public class Enrollment
    {
        public int EnrollmentID { get; set; }

        public int ModuleID { get; set; }

        public int StudentID { get; set; }

        [DisplayFormat(NullDisplayText = "No grade")]
        public Grade? Grade { get; set; }

        public virtual Module Module { get; set; }
        public virtual Student Student { get; set; }
    }
}
