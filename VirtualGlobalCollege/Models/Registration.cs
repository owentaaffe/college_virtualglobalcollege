﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public enum AcademicStatus
    {
        Undergraduate, Graduate, Postgraduate,
    }

    public class Registration
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Full name is required")]
        [Display(Name = "Full name")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please select your academic status")]
        [DisplayFormat(NullDisplayText = "Non")]
        public AcademicStatus? AcademicStatus { get; set; }

        [Required(ErrorMessage = "Full name is required")]
        [Display(Name = "Fathers name")]
        public string FatherName { get; set; }

        [Required(ErrorMessage = "Your mother's maiden name is required")]
        [Display(Name = "Mothers maiden name")]
        public string MotherName { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Place of birth is required")]
        public string Nationality { get; set; }

        [Required(ErrorMessage = "An address is required")]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [Required(ErrorMessage = "A phone number is required")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "An E-mail address is required")]
        public string Email { get; set; }

        public int CourseID { get; set; }
        public virtual Course Course { get; set; }
    }
}
