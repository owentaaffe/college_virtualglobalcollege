﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public enum Attend
    {
        Present, UnPresent
    }

    public class Attendence
    {
        public int Id { get; set; }
        public string table { get; set; }
        public string Module { get; set; }
        public string StudentName { get; set; }

        [DisplayFormat(NullDisplayText ="No Record")]
        public Attend? Lesson1 { get; set; }
        [DisplayFormat(NullDisplayText = "No Record")]
        public Attend? Lesson2 { get; set; }
        [DisplayFormat(NullDisplayText = "No Record")]
        public Attend? Lesson3 { get; set; }
        [DisplayFormat(NullDisplayText = "No Record")]
        public Attend? Lesson4 { get; set; }
        [DisplayFormat(NullDisplayText = "No Record")]
        public Attend? Lesson5 { get; set; }
        [DisplayFormat(NullDisplayText = "No Record")]
        public Attend? Lesson6 { get; set; }
        [DisplayFormat(NullDisplayText = "No Record")]
        public Attend? Lesson7 { get; set; }
        [DisplayFormat(NullDisplayText = "No Record")]
        public Attend? Lesson8 { get; set; }

    }
}
