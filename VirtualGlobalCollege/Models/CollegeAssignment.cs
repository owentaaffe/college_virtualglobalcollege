﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public class CollegeAssignment
    {              
      
        [Key]
        [ForeignKey("Instructor")]
        public int InstructorID { get; set; }

        public virtual Instructor Instructor { get; set; }

        public String CollegeName { get; set; }
    }
}
