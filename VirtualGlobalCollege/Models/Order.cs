﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualGlobalCollege.Models
{
    public partial class Order
    {

        public int OrderID { get; set; }
        public int BookID { get; set; }
        public int StudentID { get; set; }


        public int OrderNo
        {

            get
            {
                return OrderID;
            }
        }

    }
}
