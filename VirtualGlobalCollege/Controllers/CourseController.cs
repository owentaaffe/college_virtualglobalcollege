﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VirtualGlobalCollege.DAL;
using VirtualGlobalCollege.Models;

namespace VirtualGlobalCollege.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CourseController : Controller
    {
        private VirtualCollegeContext db = new VirtualCollegeContext();

        // GET: Course
        [AllowAnonymous]
        public ActionResult Index()
        {
            var courses = db.Courses.Include(c => c.Instructors);
            
            return View(courses.ToList());
        }

        public ActionResult AdminIndex()
        {
            var courses = db.Courses.Include(c => c.Instructors);
            
            return View(courses.ToList());
        }

        // GET: Course/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            ViewBag.CollegeID = new SelectList(db.Colleges, "CollegeID", "Title", course.CollegeID);
            return View(course);
        }

        // GET: Course/Create
        public ActionResult Create()
        {
            
            ViewBag.InstructorID = new SelectList(db.Instructors, "InstructorID", "FirstName");
            return View();
        }

        // POST: Course/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseID,Title,CollegeID,Description,Fee,StartDate,InstructorID")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Courses.Add(course);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CollegeID = new SelectList(db.Colleges, "CollegeID", "Title", course.CollegeID);
            ViewBag.InstructorID = new SelectList(db.Instructors, "InstructorID", "FirstName", course.InstructorID);
            return View(course);
        }

        // GET: Course/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            ViewBag.CollegeID = new SelectList(db.Colleges, "CollegeID", "Title", course.CollegeID);
            ViewBag.InstructorID = new SelectList(db.Instructors, "InstructorID", "FirstName", course.InstructorID);
            return View(course);
        }

        // POST: Course/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CourseID,Title,CollegeID,Description,Fee,StartDate,InstructorID")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CollegeID = new SelectList(db.Colleges, "CollegeID", "Title", course.CollegeID);
            ViewBag.InstructorID = new SelectList(db.Instructors, "InstructorID", "FirstName", course.InstructorID);
            return View(course);
        }

        // GET: Course/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            ViewBag.CollegeID = new SelectList(db.Colleges, "CollegeID", "Title", course.CollegeID);
            return View(course);
        }

        // POST: Course/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Course course = db.Courses.Find(id);
            db.Courses.Remove(course);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
