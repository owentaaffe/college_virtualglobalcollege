﻿using System.Linq;
using System.Web.Mvc;
using VirtualGlobalCollege.DAL;
using VirtualGlobalCollege.Models;
using System.Data.Entity;

namespace VirtualGlobalCollege.Controllers
{
    
    public class StoreController : Controller
    {
        private VirtualCollegeContext db = new VirtualCollegeContext();

        // GET: Store
        public ActionResult Index()
        {
            var course = db.Courses.Include(b => b.Books);

            return View(course.ToList());
        }

        
        public ActionResult Details(int id)
        {
            Book book = db.Books.Find(id);
            return View(book);
        }

        //[HttpPost]
        //public ActionResult Details(FormCollection form)
        //{
        //    var customerToken = form["stripeToken"];
        //   // var buyerModel = new BookViewModel();
        //   // UpdateModel(buyerModel, form.ToValueProvider());
        //    // create customer
        //    var customerOptions = new StripeCustomerCreateOptions
        //    {
        //       // Email = buyerModel.Email,
        //      //  Description = buyerModel.FullName,
        //        //TokenId = customerToken
        //    };
        //    var service = new StripeCustomerService();
        //    var customer = service.Create(customerOptions);
        //    // create a charge for the added customer
        //    var myCharge = new StripeChargeCreateOptions
        //    {
        //        Amount = 1000,
        //        Currency = "eur",
        //        CustomerId = customer.Id
        //    };
        //    var chargeService = new StripeChargeService();
        //    var stripeCharge = chargeService.Create(myCharge);
        //    return RedirectToAction("PaymentComplete", "Payment", new RouteValueDictionary { { "chargeId", stripeCharge.Id } });
        //    //Book book = db.Books.Find(id);
        //    //return View(book);
        //}

        public ActionResult Browse(string course)
        {

            var CourseModel = db.Courses.Include("Books").Single(b => b.Title == course);
            return View(CourseModel);
        }



    }
}