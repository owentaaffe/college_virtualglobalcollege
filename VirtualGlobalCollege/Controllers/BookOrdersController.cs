﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VirtualGlobalCollege.DAL;
using VirtualGlobalCollege.Models;
using VirtualGlobalCollege.ViewModels;

namespace VirtualGlobalCollege.Controllers
{
    [Authorize]
    public class BookOrdersController : Controller
    {
        private VirtualCollegeContext db = new VirtualCollegeContext();
        private BookOrderDataIndex bodi = new BookOrderDataIndex();

        // GET: BookOrders
        public ActionResult Index()
        {
            var orderDetails = db.OrderDetails.Include(o => o.Book).Include(o => o.Order).Include(o => o.Student);
            return View(orderDetails.ToList());
        }

        // GET: BookOrders
        public ActionResult BookIndex(int id)
        {
            Book book = db.Books.Find(id);
            return View(book);
        }

        // GET: BookOrders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderDetail orderDetail = db.OrderDetails.Find(id);
            if (orderDetail == null)
            {
                return HttpNotFound();
            }
            return View(orderDetail);
        }

        //// GET: BookOrders/Create
        //public ActionResult Create()
        //{
        //    ViewBag.BookID = new SelectList(db.Books, "BookID", "Title");
        //    ViewBag.OrderID = new SelectList(db.Orders, "OrderId", "FirstName");
        //    ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName");
        //    return View();
        //}

        //// POST: BookOrders/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "OrderDetailID,StudentID,OrderID,BookID,UnitPrice")] OrderDetail orderDetail)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.OrderDetails.Add(orderDetail);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.BookID = new SelectList(db.Books, "BookID", "Title", orderDetail.BookID);
        //    ViewBag.OrderID = new SelectList(db.Orders, "OrderId", "FirstName", orderDetail.OrderID);
        //    ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", orderDetail.StudentID);
        //    return View(orderDetail);
        //}

        // GET: BookOrders/Create
        public ActionResult CreateOrder()
        {
            ViewBag.BookID = new SelectList(db.Books, "BookID", "Title");
            ViewBag.OrderID = new SelectList(db.Orders, "OrderId", "FirstName");
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName");
            return View();
        }

        // POST: BookOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOrder([Bind(Include = "OrderDetailID,StudentID,OrderID,BookID,UnitPrice")] OrderDetail orderDetail)
        {
            if (ModelState.IsValid)
            {
                db.OrderDetails.Add(orderDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookID = new SelectList(db.Books, "BookID", "Title", orderDetail.BookID);
            ViewBag.OrderID = new SelectList(db.Orders, "OrderId", "FirstName", orderDetail.OrderID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", orderDetail.StudentID);
            return View(orderDetail);
        }

        // GET: BookOrders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderDetail orderDetail = db.OrderDetails.Find(id);
            if (orderDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookID = new SelectList(db.Books, "BookID", "Title", orderDetail.BookID);
            ViewBag.OrderID = new SelectList(db.Orders, "OrderId", "FirstName", orderDetail.OrderID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", orderDetail.StudentID);
            return View(orderDetail);
        }

        // POST: BookOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderDetailID,StudentID,OrderID,BookID,UnitPrice")] OrderDetail orderDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookID = new SelectList(db.Books, "BookID", "Title", orderDetail.BookID);
            ViewBag.OrderID = new SelectList(db.Orders, "OrderId", "FirstName", orderDetail.OrderID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", orderDetail.StudentID);
            return View(orderDetail);
        }

        // GET: BookOrders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderDetail orderDetail = db.OrderDetails.Find(id);
            if (orderDetail == null)
            {
                return HttpNotFound();
            }
            return View(orderDetail);
        }

        // POST: BookOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrderDetail orderDetail = db.OrderDetails.Find(id);
            db.OrderDetails.Remove(orderDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
