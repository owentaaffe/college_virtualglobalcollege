﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VirtualGlobalCollege.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TimeTable()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Please check out all the other pages too!!";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult AdminPage()
        {
            ViewBag.Message = "Admin Only Page.";

            return View();
        }

    }
}