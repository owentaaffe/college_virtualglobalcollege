﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VirtualGlobalCollege.DAL;
using VirtualGlobalCollege.Models;
using VirtualGlobalCollege.ViewModel;

namespace VirtualGlobalCollege.Controllers
{
    [Authorize(Roles = "Admin, Instructor")]
    public class InstructorDataController : Controller
    {
        private VirtualCollegeContext db = new VirtualCollegeContext();

        public ActionResult Index(int? id, int? moduleID)
        {
            var viewModel = new InstructorDataIndex();

            viewModel.Instructors = db.Instructors
                .Include(i => i.CollegeAssignments)
                .Include(i => i.Modules.Select(c => c.Course))
                .OrderBy(i => i.LastName);

            if (id != null)
            {
                ViewBag.InstructorID = id.Value;
                viewModel.Modules = viewModel.Instructors.Where(
                    i => i.InstructorID == id.Value).Single().Modules;
            }

            if (moduleID != null)
            {
                ViewBag.ModuleID = moduleID.Value;
                //viewModel.Enrollments = viewModel.Modules.Where(
                //    x => x.ModuleID == moduleID).Single().Enrollments;
                var selectedModule = viewModel.Modules.Where(x => x.ModuleID == moduleID).Single();
                db.Entry(selectedModule).Collection(x => x.Enrollments).Load();
                foreach (Enrollment enrollment in selectedModule.Enrollments)
                {
                    db.Entry(enrollment).Reference(x => x.Student).Load();
                }
                viewModel.Enrollments = selectedModule.Enrollments;
            }

            return View(viewModel);
        }
    }
}
