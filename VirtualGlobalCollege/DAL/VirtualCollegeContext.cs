﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using VirtualGlobalCollege.Models;

namespace VirtualGlobalCollege.DAL
{
    public class VirtualCollegeContext : DbContext
    {

        public VirtualCollegeContext() : base ("VirtualCollegeContext")
        {

        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<College> Colleges { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<CollegeAssignment> CollegeAssignments { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<ContactDetail> ContactDetails { get; set; }
        public DbSet<Timetable> Timetables { get; set; }
        public DbSet<Attendence> Attendences { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)

        {

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Course>().HasMany(x => x.Books);

            modelBuilder.Entity<Module>()
                .HasMany(c => c.Instructors).WithMany(i => i.Modules)
                .Map(t => t.MapLeftKey("ModuleID")
                    .MapRightKey("InstructorID")
                    .ToTable("ModuleInstructor"));
            modelBuilder.Entity<Course>().MapToStoredProcedures();
        }

        public System.Data.Entity.DbSet<VirtualGlobalCollege.Models.Survey> Surveys { get; set; }
    }
}
