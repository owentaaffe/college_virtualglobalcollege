﻿using System.Collections.Generic;
using VirtualGlobalCollege.Models;

namespace VirtualGlobalCollege.ViewModel
{
    public class InstructorDataIndex
    {
        public virtual IEnumerable<Instructor> Instructors { get; set; }
        public virtual IEnumerable<Module> Modules { get; set; }
        public virtual IEnumerable<Course> Courses { get; set; }    
        public virtual IEnumerable<Student> Students { get; set; }
        public virtual IEnumerable<ContactDetail> ContactDetails { get; set; }
        public virtual IEnumerable<Enrollment> Enrollments { get; set; }
        public virtual IEnumerable<Registration> Registrations { get; set; }
        public virtual IEnumerable<CollegeAssignment> CollegeAssignments { get; set; }
    }
}
