﻿using System.Collections.Generic;
using VirtualGlobalCollege.Models;

namespace VirtualGlobalCollege.ViewModels
{
    public class BookOrderDataIndex
    {
        public virtual IEnumerable<OrderDetail> OrderDetailS { get; set; }
        public virtual IEnumerable<Student> Students { get; set; }
        public virtual IEnumerable<Book> Books { get; set; }
    }
}
