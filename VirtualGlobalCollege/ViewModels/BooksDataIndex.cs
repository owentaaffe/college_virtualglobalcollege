﻿using System.Collections.Generic;
using VirtualGlobalCollege.Models;

namespace VirtualGlobalCollege.ViewModels
{
    public class BooksDataIndex
    {
        public virtual IEnumerable<Module> Modules { get; set; }
        public virtual IEnumerable<Book> Books { get; set; }
        public virtual IEnumerable<Course> Courses { get; set; }

    }
}
