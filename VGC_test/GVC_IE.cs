﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using RelevantCodes.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace VGC_test
{
    [TestFixture(Category = "IE, VGC Selenium Testing")]
    public class GVC_IE : ExtentReport
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new InternetExplorerDriver();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(50));
            baseURL = "http://localhost:57289/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = "<pre>" + TestContext.CurrentContext.Result.StackTrace + "</pre>";
            var errorMessage = TestContext.CurrentContext.Result.Message;

            if (status == NUnit.Framework.Interfaces.TestStatus.Failed)
            {
                test.Log(LogStatus.Fail, status + errorMessage);
            }
            extent.EndTest(test);

            driver.Close();

        }

        [Test]
        public void TheAdminloginBooklinkfindElement()
        {
            test = extent.StartTest("Test Admin login", "login as admin");

            driver.Navigate().GoToUrl(baseURL + "/");
            var login = driver.FindElement(By.Id("loginLink"));
            login.Click();
            var pwd = driver.FindElement(By.Id("Password"));
            pwd.Clear();
            pwd.SendKeys("Letmein@123");
            //driver.FindElement(By.Id("Password")).SendKeys("Letmein@123");
            var email = driver.FindElement(By.Id("Email"));
            email.Clear();
            email.SendKeys("Admin@VGC.ie");
            //driver.FindElement(By.Id("Email")).SendKeys("Admin@VGC.ie");
            var sublog = driver.FindElement(By.Id("submit_login"));
            sublog.Click();
            var adminlink = driver.FindElement(By.Id("adminpage"));
            adminlink.Click();
            var button = driver.FindElement(By.Id("BookAdminBtn"));
            button.Click();
            try
            {
                Assert.AreEqual("Add New book", driver.FindElement(By.Id("AddNewBook")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            var logout = driver.FindElement(By.Id("logoutForm"));
            logout.Click();

            test.Log(LogStatus.Info, "Validating Admin Book page link, assert element Back to List present");
        }

        [Test]
        public void TheAdminsearchenrollTest()
        {
            test = extent.StartTest("Test enrollment page", "login as admin, search enrollment/courses and find elements");

            driver.Navigate().GoToUrl(baseURL + "/Account/Login?ReturnUrl=%2FEnrollment");
            var pwd = driver.FindElement(By.Id("Password"));
            pwd.Clear();
            pwd.SendKeys("Letmein@123");
            //driver.FindElement(By.Id("Password")).SendKeys("Letmein@123");
            var email = driver.FindElement(By.Id("Email"));
            email.Clear();
            email.SendKeys("Admin@VGC.ie");
            //driver.FindElement(By.Id("Email")).SendKeys("Admin@VGC.ie");
            var login = driver.FindElement(By.Id("submit_login"));
            login.Click();
            var search = driver.FindElement(By.Id("SearchString"));
            search.Clear();
            search.SendKeys("web");
            //driver.FindElement(By.Id("SearchString")).SendKeys("web");
            driver.FindElement(By.Id("search")).Click();
            try
            {
                Assert.AreEqual("Web Applications Development", driver.FindElement(By.CssSelector("td")).Text);
                
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            driver.FindElement(By.LinkText("Log off")).Click();

            test.Log(LogStatus.Pass, "Assert Pass as condition is True");
        }

        [Test]
        public void TheRegInstructerTest()
        {
            test = extent.StartTest("Test Admin login", "login as admin");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            var pwd = driver.FindElement(By.Id("Password"));
            pwd.Clear();
            pwd.SendKeys("Letmein@123");
            var email = driver.FindElement(By.Id("Email"));
            email.Clear();
            email.SendKeys("Admin@VGC.ie");
            driver.FindElement(By.Id("submit_login")).Click();
            driver.FindElement(By.Id("adminpage")).Click();
            driver.FindElement(By.Id("RegInstruct_Admin_Btn")).Click();
            try
            {
                Assert.AreEqual("Register a new Instructor login Account.", driver.FindElement(By.CssSelector("h4")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            driver.FindElement(By.LinkText("Log off")).Click();

            test.Log(LogStatus.Info, "Validating Admin Instructer page link and find element");
        }

        [Test]
        public void TheInstructorLoginSelectinstructorSelectModuleVerifyElementTest()
        {
            test = extent.StartTest("Test Instructer login and page", "login as instructor");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            var pwd = driver.FindElement(By.Id("Password"));
            pwd.Clear();
            pwd.SendKeys("Password@123");
            var email = driver.FindElement(By.Id("Email"));
            email.Clear();
            email.SendKeys("outsidethebox@eircom.net");
            driver.FindElement(By.Id("submit_login")).Click();
            driver.FindElement(By.Id("Instlink")).Click();
            driver.Navigate().GoToUrl(baseURL + "/InstructorData");
            //driver.FindElement(By.LinkText("Select")).Click();
            driver.Navigate().GoToUrl(baseURL + "/InstructorData/Index/10");
            driver.FindElement(By.XPath("(//a[contains(text(),'Select')])[9]")).Click();
            try
            {
                Assert.AreEqual("Students Enrolled in Selected Course", driver.FindElement(By.Id("StudEnrollSelList")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            driver.FindElement(By.LinkText("Log off")).Click();

            test.Log(LogStatus.Pass, "Assert Pass as condition is True");
        }

        [Test]
        public void TheInvalidEmptyLoginTest()
        {

            test = extent.StartTest("Test Empty login", "login with no parameters");

            driver.Navigate().GoToUrl(baseURL + "/");
            var login = driver.FindElement(By.Id("loginLink"));
            login.Click();
            var submit = driver.FindElement(By.Id("submit_login"));
            submit.Click();
            try
            {

                Assert.IsTrue(IsElementPresent(By.CssSelector("span.text-danger.field-validation-error > span")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            driver.FindElement(By.Id("Homelink")).Click();

            test.Log(LogStatus.Info, "Validating empty textboxes Login and getting error message");
        }

        [Test]
        public void InvalidEmailLoginTest()
        {

            test = extent.StartTest("Test Invalid email login", "login with invalid email");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            driver.FindElement(By.Id("Email")).Clear();
            driver.FindElement(By.Id("Email")).SendKeys("letmeinim@123.com");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("password");
            driver.FindElement(By.Id("submit_login")).Click();

            try
            {
                Assert.AreEqual("Invalid login attempt.", driver.FindElement(By.CssSelector("div.validation-summary-errors.text-danger > ul > li")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            driver.FindElement(By.Id("Homelink")).Click();

            test.Log(LogStatus.Info, "InValid Student Login");
        }

        [Test]
        public void InvalidPasswordloginTest()
        {

            test = extent.StartTest("Test Invalid password login", "login with invalid password");


            driver.Navigate().GoToUrl(baseURL + "/");
            var login = driver.FindElement(By.Id("loginLink"));
            login.Click();
            var email = driver.FindElement(By.Id("Email"));
            email.Clear();
            email.SendKeys("owentaaffe@gmail.com");
            var pwd = driver.FindElement(By.Id("Password"));
            pwd.Clear();
            pwd.SendKeys("password");
            driver.FindElement(By.Id("submit_login")).Click();
            try
            {
                Assert.AreEqual("Invalid login attempt.", driver.FindElement(By.CssSelector("div.validation-summary-errors.text-danger > ul > li")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            driver.FindElement(By.Id("Homelink")).Click();

            test.Log(LogStatus.Info, "InValid Student Login with password");
        }

        [Test]
        public void TheCoursesenrollformpresentTest()
        {
            test = extent.StartTest("Test home/course and enrolment form page", "Check form element present for enrollment form");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("Courses")).Click();

            try
            {
                Assert.IsTrue(IsElementPresent(By.Id("enrollDate")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollFullName")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollAcStatus")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollFatName")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollMoName")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollNation")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollAddress")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollPhone")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollEmail")));
                Assert.IsTrue(IsElementPresent(By.Id("enrollCouID")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            try
            {
                Assert.IsTrue(IsElementPresent(By.Id("Courses")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            driver.FindElement(By.Id("Homelink")).Click();
            test.Log(LogStatus.Pass, "Assert Pass as condition is True");
        }

        [Test]
        public void TheVGCAdminLoginTest()
        {

            test = extent.StartTest("Test Admin login", "login as admin");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            var pwd = driver.FindElement(By.Id("Password"));
            pwd.Clear();
            pwd.SendKeys("Letmein@123");
            //driver.FindElement(By.Id("Password")).SendKeys("Letmein@123");
            var email = driver.FindElement(By.Id("Email"));
            email.Clear();
            email.SendKeys("Admin@VGC.ie");
            //driver.FindElement(By.Id("Email")).SendKeys("Admin@VGC.ie");
            driver.FindElement(By.Id("submit_login")).Click();
            try
            {
                Assert.AreEqual("Hello Admin@VGC.ie!", driver.FindElement(By.LinkText("Hello Admin@VGC.ie!")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);

            }

            driver.FindElement(By.Id("logoutForm")).Click();

            test.Log(LogStatus.Info, "Validating Admin Login");

        }


        [Test]
        public void TheVGCInstructorLoginTest()
        {

            test = extent.StartTest("Test Instructor login", "login as Instructor");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            var pwd = driver.FindElement(By.Id("Password"));
            pwd.Clear();
            pwd.SendKeys("Password@123");
            var email = driver.FindElement(By.Id("Email"));
            email.Clear();
            email.SendKeys("outsidethebox@eircom.net");
            driver.FindElement(By.Id("submit_login")).Click();

            try
            {
                Assert.AreEqual("Hello outsidethebox@eircom.net!", driver.FindElement(By.LinkText("Hello outsidethebox@eircom.net!")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            driver.FindElement(By.Id("logoutForm")).Click();

            test.Log(LogStatus.Info, "Validating Instructor Login");
        }

        [Test]
        public void TheVGCStudentLoginTest()
        {

            test = extent.StartTest("Test Student login", "login as Student");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            var pwd = driver.FindElement(By.Id("Password"));
            pwd.Clear();
            pwd.SendKeys("Letmein@123");
            var email = driver.FindElement(By.Id("Email"));
            email.Clear();
            email.SendKeys("owentaaffe@gmail.com");
            driver.FindElement(By.Id("submit_login")).Click();

            try
            {
                Assert.AreEqual("Hello owentaaffe@gmail.com!", driver.FindElement(By.LinkText("Hello owentaaffe@gmail.com!")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            driver.FindElement(By.Id("logoutForm")).Click();

            test.Log(LogStatus.Info, "Validating Student Login");
        }
        
        [Test]
        public void HomePage_Books_verifyMessageandLoginlink()
        {
            test = extent.StartTest("Test Admin login", "login as admin");

            driver.Navigate().GoToUrl(baseURL + "/");
            var booklist = driver.FindElement(By.Id("BookList"));
            booklist.Click();
            var link = driver.FindElement(By.LinkText("HNC in Computing and systems Developmement"));
            link.Click();
            var xpath = driver.FindElement(By.XPath("//ul[@id='book-list']/li[7]/a/span"));
            xpath.Click();
            try
            {
                Assert.IsTrue(IsElementPresent(By.CssSelector("p.text-danger > em")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            var textlink = driver.FindElement(By.LinkText("Return to book list"));
            textlink.Click();
            var css = driver.FindElement(By.CssSelector("a > span"));
            css.Click();
            try
            {
                Assert.IsTrue(IsElementPresent(By.LinkText("Login")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            var home = driver.FindElement(By.LinkText("Home"));
            home.Click();

            test.Log(LogStatus.Info, "Validating Admin Book page link, assert element Back to List present");
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}