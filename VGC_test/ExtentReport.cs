﻿using NUnit.Framework;
using RelevantCodes.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VGC_test
{
    public class ExtentReport
    {

        public ExtentReports extent;
        public ExtentTest test;

        [OneTimeSetUp]
        public void StartReport()
        {
            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string actualPath = pth.Substring(0, pth.LastIndexOf("bin"));
            string projectPath = new Uri(actualPath).LocalPath;

            string reportPath = projectPath + "Reports\\MyReport.html";

            extent = new ExtentReports(reportPath, true);

            extent.AddSystemInfo("Host Name", "owent_000")
                .AddSystemInfo("Enviroment", "Development")
                .AddSystemInfo("Tester", "Owen Taaffe");

        }

        [OneTimeTearDown]
        public void EndReport()
        {
            extent.Flush();
            extent.Close();
        }     
    }
}
