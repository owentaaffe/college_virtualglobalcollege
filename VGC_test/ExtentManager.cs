﻿using RelevantCodes.ExtentReports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VGC_test
{
    public class ExtentManager
    {
        private static ExtentReports extent;
        

        public static ExtentReports getInstance()
        {
            if (extent == null)
            {
                extent = new ExtentReports("C:\\Reports\\Report.html");

                extent.LoadConfig(Directory.GetCurrentDirectory() + "extent-config.xml");

                extent.AddSystemInfo("Selenium version", "2.53.0").AddSystemInfo("Enviroment", "Development");
            }
            return extent;
        }
    }
}
