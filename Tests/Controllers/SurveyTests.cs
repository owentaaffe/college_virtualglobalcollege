﻿using VirtualGlobalCollege.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Web.Mvc;

namespace VirtualGlobalCollege.Tests
{
    [TestFixture]   
    public class SurveyTests
    {
        [Test]
        [Ignore("Not Complied/written properly i.e not a unit test have to write an interface")]
        public void Index_ReturnView()
        {
            //Arrange
            SurveyController controller = new SurveyController();

            //Act
            var actual = controller.Index();

            //Assert
            NUnit.Framework.Assert.IsInstanceOf<ActionResult>(actual);
        }
    }
}