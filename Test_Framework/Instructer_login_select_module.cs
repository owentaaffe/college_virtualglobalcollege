﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using RelevantCodes.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Framework
{
    [TestFixture(Category = "Instructor page")]
    public class Instructer_login_select_module
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        ExtentReports extent = new ExtentReports("C:\\Reports\\InstructerPage.html");
        [SetUp]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
            baseURL = "http://localhost:57289/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            //try
            //{
                driver.Quit();
            //}
            //catch (Exception)
            //{
            //    // Ignore errors if unable to close the browser
            //}
            //Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheInstructorLoginSelectinstructorSelectModuleVerifyElementTest()
        {
            var test = extent.StartTest("Test Instructer login and page", "login as instructor");
            test.Log(LogStatus.Info, "Validating instructor Enrollmentpages and select instructer, select module (to check for student emails) and verify element with element");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("Password@123");
            driver.FindElement(By.Id("Email")).Clear();
            driver.FindElement(By.Id("Email")).SendKeys("outsidethebox@eircom.net");
            driver.FindElement(By.Id("submit_login")).Click();
            driver.FindElement(By.Id("Instlink")).Click();
            driver.Navigate().GoToUrl(baseURL + "/InstructorData");
            //driver.FindElement(By.LinkText("Select")).Click();
            driver.Navigate().GoToUrl(baseURL + "/InstructorData/Index/10");
            driver.FindElement(By.XPath("(//a[contains(text(),'Select')])[9]")).Click();
            try
            {
                Assert.IsTrue(IsElementPresent(By.XPath("//table[3]/tbody/tr[2]/td[2]")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            driver.FindElement(By.LinkText("Log off")).Click();

            extent.EndTest(test);
            extent.Flush();
            driver.Close();
            
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
