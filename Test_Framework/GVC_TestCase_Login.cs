﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using RelevantCodes.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Framework 
{
    
    [TestFixture(Category = "Valid Login")]
    public class VGCLogin 
    {
        
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        ExtentReports extent = new ExtentReports("C:\\Reports\\Validlogin.html");

        [SetUp]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
            baseURL = "http://localhost:57289/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
                driver.Close(); 
        }

        [Test]
        public void TheVGCAdminLoginTest()
        {
            //ExtentReports extent = new ExtentReports("C:\\ReportS\\AdminLogin.html");
            var test = extent.StartTest("Test Admin login", "login as admin");
            test.Log(LogStatus.Info, "Validating Admin Login");
            try
            {
                driver.Navigate().GoToUrl(baseURL + "/");
                driver.FindElement(By.Id("loginLink")).Click();
                driver.FindElement(By.Id("Password")).Clear();
                driver.FindElement(By.Id("Password")).SendKeys("Letmein@123");
                driver.FindElement(By.Id("Email")).Clear();
                driver.FindElement(By.Id("Email")).SendKeys("Admin@VGC.ie");
                driver.FindElement(By.Id("submit_login")).Click();
                
                Assert.AreEqual("Hello Admin@VGC.ie!", driver.FindElement(By.LinkText("Hello Admin@VGC.ie!")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);

            }

            driver.FindElement(By.Id("logoutForm")).Click();

            extent.EndTest(test);
            extent.Flush();
            driver.Close();
        }


        [Test]
        public void TheVGCInstructorLoginTest()
        {
            //ExtentReports extent = new ExtentReports("C:\\ReportS\\Instructlogin.html");
            var test = extent.StartTest("Test Instructor login", "login as Instructor");
            test.Log(LogStatus.Info, "Validating Instructor Login");

            test.Log(LogStatus.Info, "Validating Admin Login");
            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("Password@123");
            driver.FindElement(By.Id("Email")).Clear();
            driver.FindElement(By.Id("Email")).SendKeys("outsidethebox@eircom.net");
            driver.FindElement(By.Id("submit_login")).Click();
            
            try
            {
                Assert.AreEqual("Hello outsidethebox@eircom.net!", driver.FindElement(By.LinkText("Hello outsidethebox@eircom.net!")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            driver.FindElement(By.Id("logoutForm")).Click();

            extent.EndTest(test);
            extent.Flush();
            driver.Close();
        }

        [Test]
        public void TheVGCStudentLoginTest()
        {
            //ExtentReports extent = new ExtentReports("C:\\ReportS\\Studentlogin.html");
            var test = extent.StartTest("Test Student login", "login as Student");
            test.Log(LogStatus.Info, "Validating Student Login");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("Letmein@123");
            driver.FindElement(By.Id("Email")).Clear();
            driver.FindElement(By.Id("Email")).SendKeys("owentaaffe@gmail.com");
            driver.FindElement(By.Id("submit_login")).Click();
           
            try
            {
                Assert.AreEqual("Hello owentaaffe@gmail.com!", driver.FindElement(By.LinkText("Hello owentaaffe@gmail.com!")).Text);
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            driver.FindElement(By.Id("logoutForm")).Click();

            extent.EndTest(test);
            extent.Flush();
            driver.Close();
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }

}
