﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using RelevantCodes.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Framework
{
    [TestFixture(Category ="Invalid Login")]
    public class InvalidLogin 
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        ExtentReports extent = new ExtentReports("C:\\Reports\\Invalidlogin.html");

        [SetUp]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
            baseURL = "http://localhost:57289/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            driver.Close();
        }

        [Test]
        public void TheInvalidEmptyLoginTest()
        {
            var test = extent.StartTest("Test Empty login", "login with no parameters");
            test.Log(LogStatus.Info, "Validating empty textboxes Login and getting error message");
            try
            {
                driver.Navigate().GoToUrl(baseURL + "/");
                driver.FindElement(By.Id("loginLink")).Click();
                driver.FindElement(By.Id("submit_login")).Click();

                Assert.IsTrue(IsElementPresent(By.CssSelector("span.text-danger.field-validation-error > span")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }
            driver.FindElement(By.Id("Homelink")).Click();

            extent.EndTest(test);
            extent.Flush();
            driver.Close();
        }

        [Test]
        public void InvalidEmailLoginTest()
        {
            
            var test = extent.StartTest("Test Invalid email login", "login with invalid email");
            test.Log(LogStatus.Info, "InValid Student Login");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            driver.FindElement(By.Id("Email")).Clear();
            driver.FindElement(By.Id("Email")).SendKeys("letmeinim@123.com");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("password");
            driver.FindElement(By.Id("submit_login")).Click();

            try
            {
                Assert.IsTrue(IsElementPresent(By.CssSelector("div.validation-summary-errors.text-danger > ul > li")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            driver.FindElement(By.Id("Homelink")).Click();

            extent.EndTest(test);
            extent.Flush();
            driver.Close();
        }

        [Test]
        public void InvalidPasswordloginTest()
        {

            //ExtentReports extent = new ExtentReports("C:\\ReportS\\Reports.html");
            var test = extent.StartTest("Test Invalid password login", "login with invalid password");
            test.Log(LogStatus.Info, "InValid Student Login with password");

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            driver.FindElement(By.Id("Email")).Clear();
            driver.FindElement(By.Id("Email")).SendKeys("owentaaffe@gmail.com");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("password");
            driver.FindElement(By.Id("submit_login")).Click();
            try
            {
                Assert.IsTrue(IsElementPresent(By.CssSelector("div.validation-summary-errors.text-danger > ul > li")));
            }
            catch (AssertionException e)
            {
                verificationErrors.Append(e.Message);
            }

            driver.FindElement(By.Id("Homelink")).Click();

            extent.EndTest(test);
            extent.Flush();
            driver.Close();
        }


        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}